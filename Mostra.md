# Document de mostra jcasado:
***
# Capçaleres

# Capçalera 1
## Capçalera 2
### Capçalera 3
#### Capçalera 4
##### Capçalera 5
***
# Format de text
+ **Això està escrit en negreta**
+ _Això està escrit en cursiva_
+ ***Això està escrit en negreta i cursiva***
+ ~Això està tatxat~
***
# Taules
+ Ara introduiu una taula

Blancs | Blaus
-- | --   
Estirada de corda  |  Rajoles
Construcció de gegants  |  Descans  

***  
# Llistes
## Llista ordenada amb subapartats
1. Colors
2. Blau
3. Groc
4. Persones
   1. Homes
   2. Dones

## Llista no ordenada amb subllistes
+ Colors
  - Blau
  - Groc
+ Persones
  - Homes
  - Dones

## Llista "checklist"
+ [ ] Colors
  - [ ] Blau
  - [ ] Groc
+ [x] Persones
  - [ ] Homes
  - [x] Dones

***
# Cites
Com va dir Confucio:
> Si un problema te solució, per que et preocupes? Si un problema no te solució, per que et preocupes?

***
# Enllaços a recursos externs (links)

[Click aqui per anar a Youtube](http://youtube.com "youtube"). Això es text normal.

## Enllaços a imatges
[Imatge a internet](https://www.google.com/imgres?imgurl=http%3A%2F%2F2.bp.blogspot.com%2F-AGk8cmdxvR0%2FVIc-c5fHXnI%2FAAAAAAAAAUk%2FBf4C71bPBnw%2Fs1600%2F1-1238813_26934058.jpg&imgrefurl=https%3A%2F%2Falltechspain.blogspot.com%2F2014%2F12%2Flas-vacas-con-nombre-producen-mas.html&tbnid=-Lc7_6-EzKehaM&vet=12ahUKEwjAs8bunIXsAhXL5eAKHYRZCR4QMygBegUIARClAQ..i&docid=LHMkNXMg-gSAYM&w=1600&h=1071&q=vaca&client=ubuntu&ved=2ahUKEwjAs8bunIXsAhXL5eAKHYRZCR4QMygBegUIARClAQ)

![Goma](/home/usuari/Baixades/09-Goma.png)

***
# Introduir Codi
Codi en la mateixa línia de text `var code = 1`
+ Aquí teniu un tros de codi en python:
~~~
x = 1
if x = 1
    # indented four spaces
    print("x is 1.")
~~~